titanic<-read.csv("titanic.csv")
#Q1 - 
str(titanic)
#are there ant NA's in our dataset? TRUE = yes!

is.na(titanic)
#missing values: age - we will fill in all the NA's with the mean of the ages.
titanic$Age
#lets find the mean of the ages 
agemean<-mean(titanic$Age,na.rm = T)
#fill the missing values with the mean value of that attribute
titanic$Age[is.na(titanic$Age)]<-agemean

#Q2 - 
#Q3 - change Survived to factor
titanic$Survived<-factor(titanic$Survived,levels = c(0,1),lables<-c("No","Yes"))

#Q4 - Use ggplot histogram to investigate how Age, Fare and SibSp affect Survival
library(ggplot2)
p1<-ggplot(titanic,aes(x=Age, fill=Survived))
p1<-p1+geom_histogram(binwidth = 0.5)

p2<-ggplot(titanic,aes(x=Fare, fill=Survived))+geom_histogram(binwidth = 0.5) 

p3<-ggplot(titanic,aes(x=SibSp, fill=Survived))+geom_histogram(binwidth = 0.5)


#Q5 - Select relevant features for machine learnig
#Age,SibSp, Fare, Sex

